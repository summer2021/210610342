项目名称：实现一个 MindSpore 图层 IR 融合优化 pass
- 项目背景
·Mindspore 简介
随着近年来深度学习的快速发展，国外大的互联网公司谷歌（Google）和脸书（Facebook）分别推出了自己的深度学习框架：采用静态计算图的Tensorflow与采用动态图的Pytorch,二者都受到了广泛的应用。国内华为公司推出的“昇思”(Mindspore)，结合了动静态图，发挥了二者的优势。

·图算融合简介
图算融合是MindSpore特有的网络性能优化技术，在静态模式-Graph模式下运行，与同样采用静态计算图的Tensorflow相比，“昇思”(Mindspore)具有以下优势：
“_它可以通过自动分析和优化现有网络计算图逻辑，并结合目标硬件能力，对计算图进行计算化简和替代、算子拆分和融合、算子特例化编译等优化，以提升设备计算资源利用率， 实现对网络性能的整体优化。相比传统优化技术，图算融合具有多算子跨边界联合优化、与 MindAKG（基于Polyhedral的算子编译器）跨层协同、即时编译等独特优势。另外，图算融 合只需要用户打开对应配置后，整个优化过程即可自动完成，不需要网络开发人员进行其它 额外感知，使得用户可以聚焦网络算法实现_。”
 
图算融合的适用场景包括：
①_对网络执行时间具有较高性能要求的场景_；
②_通过拼接基本算子实现自定义组合算子，并希望对这些基本算子进行自动融合，以提升自定义组合算子性能的场景_。
基于图算融合的实际应用价值，我们有必要对图编译过程中进行图算融合优化，对应的应用方案即本项目课题-编写识别特定结构的图层 IR融合优化pass。
  
- 方案描述：
根据项目要求，实现一个图层 IR 融合优化 pass。在图的编译阶段，通过各个 pass 进 行优化，每个 pass 将识别到的图中特定结构，识别成功后进一步进行算子替换，将原网络 图中特定的结构中的算子进行融合，优化替换为相应更简单结构，从而实现网络图结构的优 化。本次项目的图层融合优化 pass 是实现一个 prelu 的融合优化 pass，将 neg,relu,mul,add 算 子组成的特定结构进行融合优化为 prelu 算子结构，具体的优化前特定算子结构如下图所示。
![Image text](https://gitlab.summer-ospp.ac.cn/summer2021/210610342/-/blob/main/image/%E4%BC%98%E5%8C%96%E5%89%8D%E7%BB%93%E6%9E%84.png)
                                                            

为实现上图结构的优化，在 ir_fusion 文件中添加编写的优化 pass,即 prelu_fusion,实现的功 
能具体为识别上图结构，将输入 x,weight 通过算子 prelu 运算从而转换为新的网络结构，并输出新的网络图结点。在图编译的过程中，通过 compilegrahimpl 函数的调用，将运行各类 
优化 pass，将编写的 prelu_fusion 优化 pass 添加在 ascend_backend_optimization.cc 调用的 
pass 序列中，即可实现 prelu 的优化 pass。具体的优化后的网络图结构如下图所示。
![Image text](https://gitlab.summer-ospp.ac.cn/summer2021/210610342/-/blob/main/image/%E4%BC%98%E5%8C%96%E5%90%8E%E7%BB%93%E6%9E%84.png)

